#!/usr/bin/env python

import os
import uuid
import shutil
import time
import json
import requests
import argparse
import getpass
import avro.schema

# from hdfs import InsecureClient
# from hdfs import Config
# from hdfs.ext.avro import AvroWriter
import confluent_kafka
from confluent_kafka import Consumer
from confluent_kafka import KafkaError
from confluent_kafka.avro import AvroConsumer
from confluent_kafka.avro.serializer import SerializerError

from experiment_parse import ExperimentState


# parse command line args
parser = argparse.ArgumentParser(description='Write all data from a single experiment to HDFS')
parser.add_argument('--delay', type=int, default=10800,
                    help="the delay between new experiments (default: 10800)")

# HDFS config
# parser.add_argument('--nodename', type=str, default="hnn1-americano.di.uoa.gr",
#                     help="the default node to use (default: hnn1-americano.di.uoa.gr)")
# parser.add_argument('--port', type=int, default=50070,
#                     help="the default port for hdfs (default: 50070)")
# parser.add_argument('--user', type=str, default=None,
#                     help="the username for hdfs (default: None)")

# kafka + schema registry config
parser.add_argument('--bootstrap-servers', type=str, default="eagle5.di.uoa.gr:9092",
                    help="the default bootstrap server(s) (default: eagle5.di.uoa.gr:9092)")
parser.add_argument('--schema-registry', type=str, default="http://eagle5.di.uoa.gr:8081",
                    help="location of schema registry (default: http://eagle5.di.uoa.gr:8081)")
args = parser.parse_args()


def get_current_user():
    ''' helper to get the current user acct'''
    return getpass.getuser()


# def establish_client(args):
#     ''' establish an insecure HDFS connection '''
#     host_str = 'http://{}:{}'.format(
#         args.nodename,
#         args.port
#     )
#     user_str = args.user if args.user is not None else get_current_user()
#     return InsecureClient(host_str, user=user_str)


def grab_schema(name, version=None):
    ''' grabs the latest schema from the registry if version=None,
        otherwise grabs version '''
    schema_base_path = '{}/subjects/{}/versions'.format(args.schema_registry, name)
    if version is None: # get the latest version
        version = requests.get(schema_base_path).json()[-1]

    # grab the entire full schema
    schema_json = requests.get(schema_base_path + '/{}'.format(str(version)))
    schema_json.encoding = 'utf-8'
    schema_json = schema_json.json()
    return avro.schema.Parse(schema_json['schema'])


#TODO: write some logic for hashing a message to a file;
#      this will allow us to re-start the writing process
#      from a specific index.


def get_all_schema_names(args):
    ''' get all the schemas from the registry '''
    schema_path = '{}/subjects/'.format(args.schema_registry)
    schema_json = requests.get(schema_path)
    schema_json.encoding = 'utf-8'
    schema_json = schema_json.json()
    print('original length of subjects = ', len(schema_json))
    for i in range(len(schema_json)):
        schema_json[i] = schema_json[i].replace("-key", "")
        schema_json[i] = schema_json[i].replace("-value", "")

    schema_json = list(set(schema_json))
    print('pruned key/values length = ', len(schema_json))
    return schema_json

def get_all_schema_names_hack(args):
    ''' get all the schemas from the registry '''
    schema_path = '{}/subjects/'.format(args.schema_registry)
    schema_json = requests.get(schema_path)
    schema_json.encoding = 'utf-8'
    schema_json = schema_json.json()
    print('original length of subjects = ', len(schema_json))
    schema_json = [sj for sj in schema_json if '-value' in sj]
    schema_json = list(set(schema_json))
    print('pruned key/values length = ', len(schema_json))
    return schema_json


def handle_msg(client, state, schema, msg):
    ''' takes a message, a hdfs client, a experiment state and
        a schema and writes to hdfs and a local folder '''
    # handle naming
    exp_name = state(schema, msg)
    # if exp_name is None:
    #     print("no experiment detected for {} msg, waiting for msg!".format(schema))
    #     return
    
    #unique_uuid = "{}_{}.avro".format(schema, str(uuid.uuid4()))
    unique_uuid = "{}_{}.json".format(schema, str(uuid.uuid4()))
    full_msg_path = "/storage/experiment{}/{}".format(exp_name, unique_uuid)

    # write the msg or msgs as avro --> currently writing 0 bytes
    # with AvroWriter(client, full_msg_path) as writer:
    #     if isinstance(msg, list):
    #         for m in msgs:
    #             writer.write(m)
    #     else:
    #         writer.write(msg)

    # write the msgs as json --> also writing 0 bytes
    # NOTE: This is currently writing 0 bytes
    # with client.write(full_msg_path, encoding='utf-8') as writer:
    #     json.dump(msg, writer)

    # write raw msg to hdfs --> also fails
    # client.write(full_msg_path, json.dumps(msg))    

    # for RAW handling
    # with open("." + full_msg_path, encoding='utf-8', mode="w") as file_writer:        
    #     with AvroWriter(file_writer, full_msg_path) as writer:
    #         if isinstance(msg, list):
    #             for m in msgs:
    #                 writer.write(m)
    #         else:
    #             writer.write(msg)

    # do some local writes
    # exp_local_dir_name = "./storage/experiment{}".format(exp_name)
    # if not os.path.isdir(exp_local_dir_name):
    #     os.makedirs(exp_local_dir_name)
    
    # with open("." + full_msg_path, encoding='utf-8', mode="w") as file_writer:
    #     json.dump(msg, file_writer)


def assign_to_stored(consumer, partitions):
    for p in partitions:
        p.offset = confluent_kafka.OFFSET_STORED
        # p.offset = confluent_kafka.OFFSET_END
        print("assign: ", p)

    consumer.assign(partitions)

    
def run(args):
    # grab a list of the available schemas registered
    # this filters out all -key, -value pairs and returns a set
    # of unique ones, eg: Exp-key, Exp-value --> Exp
    # all_schemas = get_all_schema_names_hack(args)
    all_schemas = get_all_schema_names(args)

    # XXX, only subscribe to startRequests, cancelRequests & statusMsg
    all_schemas = [item for item in all_schemas if 'Experiment' in item]
    print("subscribed to : ", all_schemas)
    
    # build the consumer and register to all schemas
    consumer = AvroConsumer({
        'bootstrap.servers': args.bootstrap_servers,
        'group.id': 'hdfsConsumerTest0',
        # 'auto.offset.reset': 'latest',  # when it resets what offset should it reset to?
        'schema.registry.url': args.schema_registry})

    # Create a RAW consumer
    # consumer = Consumer({
    #     'bootstrap.servers': args.bootstrap_servers,
    #     'group.id': 'pyConsumer1',
    #     'auto.offset.reset': 'earliest'})

    # subscribe to all the schemas from above
    consumer.subscribe(all_schemas, on_assign=assign_to_stored)

    # build the HDFS client
    # client = establish_client(args)
    client = None

    # build a simple experiment heuristic
    #experiment_state = ExperimentState(delay_between_experiments_sec=args.delay)
    experiment_state = ExperimentState()

    while True: # persistence
        try:
            msg = consumer.poll(1.0)
        except SerializerError as e:
            print("deserializaion failed for {}: {}\n".format(msg, e))
            with open("errors.log", "a") as log:
                log.write("deserializaion failed for {}: {}\n".format(msg, e))

            continue

        if msg is None:
            continue

        if msg.error():
            print("consumer error: {}".format(msg.error()))
            continue

        # handle the message in AVRO
        print('Received msg for {} : {}'.format(msg.key(), msg.value())) # DEBUG
        handle_msg(client, experiment_state, msg.key(), msg.value())

        # handle the RAW message
        # print('Received msg : {}'.format(msg.value())) # DEBUG
        # handle_msg(client, experiment_state, "None", msg.value())

    # handle cleanups
    consumer.close()


if __name__ == "__main__":
    run(args)
