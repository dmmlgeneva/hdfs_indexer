#!/usr/bin/env python

import os
import json
import requests
import argparse
import getpass
import avro.schema


from confluent_kafka import KafkaError
from confluent_kafka.avro import AvroConsumer
from confluent_kafka.avro.serializer import SerializerError


# parse command line args
parser = argparse.ArgumentParser(description='Consumer Example')
parser.add_argument('--bootstrap-servers', type=str, default="eagle5.di.uoa.gr:9092",
                    help="the default bootstrap server(s) (default: eagle5.di.uoa.gr:9092)")
parser.add_argument('--schema-registry', type=str, default="http://eagle5.di.uoa.gr:8081",
                    help="location of schema registry (default: http://eagle5.di.uoa.gr:8081)")
parser.add_argument('--topic', type=str, default="ExperimentStartRequest",
                    help="the topic to subscribe to (default: ExperimentStartRequest)")
args = parser.parse_args()


def grab_schema(name, version=None):
    ''' grabs the latest schema from the registry if version=None,
        otherwise grabs version '''
    schema_base_path = '{}/subjects/{}/versions'.format(args.schema_registry, name)
    if version is None: # get the latest version
        version = requests.get(schema_base_path).json()[-1]

    # grab the entire full schema
    schema_json = requests.get(schema_base_path + '/{}'.format(str(version)))
    schema_json.encoding = 'utf-8'
    schema_json = schema_json.json()
    return avro.schema.Parse(schema_json['schema'])


# the start conditions are true
exp_start_map = {
    'ONGOING' : True,
    'BOOKED': True,
    'CHANGED': True,
    'COMPLETED': False,
    'BLOCKED': False,
    'CANCELED': False,
    'FAILED': False
}


def run(args):
    # grab the status and start messages
    # status_msgs = parse_experiment_schemas(client, args.status_schema_path, args)
    # start_msgs = parse_experiment_schemas(client, args.start_schema_path, args)

    consumer = AvroConsumer({
        'bootstrap.servers': args.bootstrap_servers,
        'group.id': 'pyConsumer1',
        'schema.registry.url': args.schema_registry})
    consumer.subscribe([args.topic])

    while True:
        try:
            msg = consumer.poll(10)
        except SerializerError as e:
            print("deserializaion failed for {}: {}".format(msg, e))
            break

        if msg is None:
            continue

        if msg.error():
            print("consumer error: {}".format(msg.error()))
            continue

        print(msg.value())

    consumer.close()


if __name__ == "__main__":
    run(args)
