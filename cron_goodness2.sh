# Run though the dirs in storage/ and copy them to hdfs
for d in $(ls /home/hadrawfie/hdfs_indexer/storage ) ; do
    echo "Copying $d to HDFS"
    #nohup hadoop fs -copyFromLocal /home/hadrawfie/hdfs_indexer/storage/$d /storage_local_copy/ >/dev/null 2>&1 &
    hadoop fs -copyFromLocal /home/hadrawfie/hdfs_indexer/storage/$d /storage_local_copy/
done
