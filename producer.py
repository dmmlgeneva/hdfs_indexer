#!/usr/bin/env python

import os
import json
import requests
import argparse
import getpass
import numpy as np

from avro.io import Validate
import avro.schema as avs
# from avro.datafile import DataFileWriter
# from avro.io import DatumWriter

from confluent_kafka import avro
from confluent_kafka.avro import AvroProducer

# parse command line args
parser = argparse.ArgumentParser(description='Producer Example')
parser.add_argument('--bootstrap-servers', type=str, default="eagle5.di.uoa.gr:9092",
                    help="the default bootstrap server(s) (default: eagle5.di.uoa.gr:9092)")
parser.add_argument('--schema-registry', type=str, default="http://eagle5.di.uoa.gr:8081",
                    help="location of schema registry (default: http://eagle5.di.uoa.gr:8081)")
parser.add_argument('--topic', type=str, default="ExperimentStartRequest",
                    help="the topic to subscribe to (default: ExperimentStartRequest)")
args = parser.parse_args()


value_schema_str = """
{
    "type": "record",
    "name": "ExperimentStartRequest",
    "namespace": "eu.rawfie.general.service.types",
    "fields":[
    {
        "name": "executionId",
        "type":
        {
            "type": "string",
            "avro.java.string": "String"
        }
    },
    {
        "name":"script",
        "type":
        {
            "type":"string",
            "avro.java.string":"String"
        }
    },
    {
        "name":"testbedId",
        "type":
        {
            "type": "string",
            "avro.java.string": "String"
        }
    },
    {
        "name": "isIndoor",
        "type": "boolean",
        "default": false
    },
    {
        "name": "resourceNames",
        "type":
        {
            "type": "array",
            "items":"string"
        }
    },
    {
        "name": "partitionids",
        "type":
        {
            "type": "array",
            "items": "int"
        }
    },
    {
        "name": "accuracy",
        "type": "double",
        "doc": "Desired accuracy for the waypoint-navigation procedure",
        "unit": "m"
    },
    {
        "name": "takeOffHeights",
        "type":[ "null",
        {
            "type": "array",
            "items":"double"
        }],
        "doc": "Desired take off height - For UAVs only",
        "default": null,
        "unit": "m"
    }]
}
"""

# key_schema_str = """
# {
#    "namespace": "my.test",
#    "name": "key",
#    "type": "record",
#    "fields" : [
#      {
#        "name" : "name",
#        "type" : "string"
#      }
#    ]
# }
# """


def grab_schema(name, version=None):
    ''' grabs the latest schema from the registry if version=None,
        otherwise grabs version '''
    schema_base_path = '{}/subjects/{}/versions'.format(args.schema_registry, name)
    if version is None: # get the latest version
        version = requests.get(schema_base_path).json()[0]

    print("using schema version [{}]".format(version))

    # grab the entire full schema
    schema_json = requests.get(schema_base_path + '/{}'.format(str(version)))
    schema_json.encoding = 'UTF-8'
    schema_json = schema_json.json()
    # print(json.dumps(schema_json, indent=4, sort_keys=True))
    return avs.Parse(schema_json['schema'])

    # return avro.loads(value_schema_str)


# def generate_random_data():
#     from dataclasses import dataclass

#     @dataclass(frozen=True)
#     class ExperimentStartRequest:
#         executionId: str
#         script: list
#         testbedId: str
#         isIndoor: list
#         resourceNames: list
#         partitionids: list
#         accuracy: list
#         takeOffHeights: list

#     return ExperimentStartRequest(
#         executionId='testHDFSSerializationV0',
#         script=np.random.choice(['/bin/true', '/bin/false']),
#         testbedId='Geneva',
#         isIndoor=np.random.choice([False, True]),
#         resourceNames= np.random.choice([['uav'], ['uxv', 'usv']]),
#         partitionids=[np.random.randint(25)],
#         accuracy=np.random.choice([1e-3, 1e-4, 1e-6]),
#         takeOffHeights=np.random.choice([[100.], [10., 100.], [20.]])
#     )

def generate_random_data():
    return {
        'executionId': 'testHDFSSerializationV0',
        'script': str(np.random.choice(['/bin/true', '/bin/false'])),
        'testbedId': 'Geneva',
        'isIndoor': bool(np.random.choice([False, True])),
        'resourceNames': list(np.random.choice([['uav'], ['uxv', 'usv']])),
        'partitionids': list([np.random.randint(25)]),
        'accuracy': float(np.random.choice([1e-3, 1e-4, 1e-6])),
        'takeOffHeights': list(np.random.choice([[100.], [10., 100.], [20.]]))
    }


def run(args):
    # grab the status and start messages
    # status_msgs = parse_experiment_schemas(client, args.status_schema_path, args)
    # start_msgs = parse_experiment_schemas(client, args.start_schema_path, args)
    key_schema = grab_schema(args.topic+"-key")
    value_schema = grab_schema(args.topic+"-value")#, )
    # from pprint import pprint
    # pprint(vars(value_schema))

    # value = {"name": "Value"}
    key = {"name": "Key"}
    value = generate_random_data()
    # print('valid? ', Validate(value_schema, value))
    # exit(0)

    # create a producer object
    avroProducer = AvroProducer({
        'bootstrap.servers': args.bootstrap_servers,
        'schema.registry.url': args.schema_registry
    })

    # avroProducer = AvroProducer({
    #     'bootstrap.servers': args.bootstrap_servers,
    #     'schema.registry.url': args.schema_registry
    # }, default_key_schema=key_schema, default_value_schema=value_schema)
    avroProducer = AvroProducer({
        'bootstrap.servers': args.bootstrap_servers,
        'schema.registry.url': args.schema_registry
    }, default_value_schema=value_schema,
    default_key_schema=key_schema)
    # default_key_schema=avro.loads('{"type": "string"}'))

    # push the actual data
    avroProducer.produce(topic=args.topic, value=value, key='key')
    avroProducer.flush()

if __name__ == "__main__":
    run(args)
