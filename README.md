# HDFS Pipe Tool

There are three components to this tool:

  1. HDFS Pipe: `write_all_consumer.py` --> reads all data from schema registry and tries to write to HDFS and Local dir (default at /storage)
  2. Example consumer
  3. Example producer


## Usage

``` bash
python write_all_consumer.py -h
```

The main parameterization besides the server endpoints are the time between experiments.
The biggest limitation of this is that experiments are SERIAL and are time-lagged between each other by K-seconds (parameterizable, see help).

Errors for un-parsable schemas are stored in `errors.log`

## Example consumer

See consumer.py

``` bash
python consumer.py --topic ExperimentStartRequestV2 --schema-registry http://eagle12.di.uoa.gr:8081 --bootstrap-servers eagle12.di.uoa.gr:9092
```


## Example producer

See producer.py

``` bash
python producer.py --topic ExperimentStartRequest --schema-registry http://eagle5.di.uoa.gr:8081 --bootstrap-servers eagle5.di.uoa.gr:9092
```
