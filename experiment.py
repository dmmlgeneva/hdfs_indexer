import os
import uuid
import shutil
import json
import time
import tarfile


def tar_folder(source_dir):
    ''' helper to zip the root of this git-dir'''
    output_filename="{}.tar.gz".format(source_dir)
    print("compressing {}..".format(source_dir), end='', flush=True)
    with tarfile.open(output_filename, "w:gz") as tar:
        tar.add(source_dir, arcname=os.path.basename(source_dir))

    print("completed!")
    if os.path.isdir(source_dir):
        print("removing {} directory...".format(source_dir))
        shutil.rmtree(source_dir)


class ExperimentState(object):
    def __init__(self):
        # self.exp_name = str(uuid.uuid4())[:8]
        self.exp_name = None
        self.state = None

    def __call__(self, schema, msg):
        # if 'testbedId' in msg:        
        if (schema == 'ExperimentLaunchRequest' or schema =='ExperimentCancelRequest'
            or schema == 'ExperimentStartRequest' or schema == 'ExperimentStatusMsg') and 'executionId' in msg:
            is_reset = False

            # handle logic for ExperimentStatusMsg
            if schema == 'ExperimentStatusMsg': #and self.state is not None and self.state != msg["status"]:
                self.state = msg["status"]
                if self.state not in ["COMPLETED", "CANCELED"] and self.exp_name is not None:
                    return self.exp_name # ignore any message besides COMPLETED
                else: # We have completed an experiment, reset to compress
                    is_reset = True

            # handle logic for ExperimentCancelRequest
            if schema == 'ExperimentCancelRequest':
                is_reset = True

            # handle logic to remove directory and tar                
            if is_reset:
                exp_local_dir = "./storage/experiment{}".format(self.exp_name)
                if os.path.isdir(exp_local_dir):
                    tar_folder(exp_local_dir)

                self.exp_name = None
                return self.exp_name
            
            self.exp_name = msg['executionId']
            
        return self.exp_name
        
# class ExperimentState(object):
#     def __init__(self, delay_between_experiments_sec=10800):
#         self.delay = delay_between_experiments_sec
#         self.reset()

#     def reset(self):
#         ''' reset timer '''
#         self.init_time = time.time()
#         self.generate_new_name()

#     def generate_new_name(self):
#         self.exp_name = str(uuid.uuid4())[:8]
#         return self.exp_name

#     def get_name(self):
#         return self.exp_name

#     def __call__(self):
#         ''' simple returns True or False if the experiment
#             is now a new one based on a simple time heuristic '''
#         if time.time() - self.init_time > self.delay:
#             self.reset()

#         return self.exp_name
